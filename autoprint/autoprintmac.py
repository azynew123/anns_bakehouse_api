import os
import time
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class Watcher:
    def __init__(self, directory_to_watch):
        self.directory_to_watch = directory_to_watch
        self.observer = Observer()

    def run(self):
        event_handler = Handler(self.directory_to_watch)
        self.observer.schedule(event_handler, self.directory_to_watch, recursive=True)
        self.observer.start()
        print(f"Observer dijalankan untuk folder: {self.directory_to_watch}")
        try:
            while True:
                time.sleep(5)
        except KeyboardInterrupt:
            self.observer.stop()
            print("Observer dihentikan")
        self.observer.join()

class Handler(FileSystemEventHandler):
    def __init__(self, directory_to_watch):
        self.directory_to_watch = directory_to_watch

    def on_created(self, event):
        print(f"Peristiwa terdeteksi: {event}")
        if event.is_directory:
            print("Peristiwa diabaikan karena direktori")
            return None
        else:
            print(f"Menunggu sebentar untuk memeriksa file: {event.src_path}")
            time.sleep(5)  # Waktu tunggu untuk memastikan file PDF telah selesai diunduh
            self.check_for_pdfs()

    def check_for_pdfs(self):
        for filename in os.listdir(self.directory_to_watch):
            if filename.endswith(".pdf"):
                file_path = os.path.join(self.directory_to_watch, filename)
                print(f"File PDF baru ditemukan: {file_path}")
                print_pdf(file_path, desired_printer)  # Menggunakan printer default yang telah ditentukan
                os.remove(file_path)
                print(f"File dihapus: {file_path}")

def set_default_printer(printer_name):
    try:
        subprocess.run(['lpoptions', '-d', printer_name], check=True)
        print(f"Printer default diatur ke: {printer_name}")
    except subprocess.CalledProcessError as e:
        print(f"Error saat mengatur printer default: {e}")

def print_pdf(file_path, printer_name):
    # Menggunakan lp command untuk mencetak di macOS dengan ukuran kertas A5, 2 salinan, not collated
    print_command = f'lp -d "{printer_name}" -o media=A5 -n 2 -o collate=false "{file_path}"'
    print(f"Menjalankan perintah print: {print_command}")
    try:
        result = subprocess.run(print_command, shell=True, capture_output=True, text=True)
        print(f"Output perintah: {result.stdout}")
        if result.stderr:
            print(f"Error perintah: {result.stderr}")
    except Exception as e:
        print(f"Error saat menjalankan perintah print: {e}")

if __name__ == '__main__':
    # Ganti dengan path folder unduhan Anda di macOS
    path_to_watch = '/Users/imac/Downloads'

    # Ganti dengan nama printer yang ingin dijadikan default
    desired_printer = "Canon_LBP6030_6040_6018L"
    set_default_printer(desired_printer)

    w = Watcher(path_to_watch)
    w.run()

    
    

    
