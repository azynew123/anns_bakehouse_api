import os
import time
import win32print
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class Watcher:
    def __init__(self, directory_to_watch):
        self.directory_to_watch = directory_to_watch
        self.observer = Observer()

    def run(self):
        event_handler = Handler(self.directory_to_watch)
        self.observer.schedule(event_handler, self.directory_to_watch, recursive=True)
        self.observer.start()
        print(f"Observer dijalankan untuk folder: {self.directory_to_watch}")
        try:
            while True:
                time.sleep(5)
        except KeyboardInterrupt:
            self.observer.stop()
            print("Observer dihentikan")
        self.observer.join()

class Handler(FileSystemEventHandler):
    def __init__(self, directory_to_watch):
        self.directory_to_watch = directory_to_watch

    def on_created(self, event):
        print(f"Peristiwa terdeteksi: {event}")
        if event.is_directory:
            print("Peristiwa diabaikan karena direktori")
            return None
        else:
            print(f"Menunggu sebentar untuk memeriksa file: {event.src_path}")
            time.sleep(5)  # Waktu tunggu untuk memastikan file PDF telah selesai diunduh
            self.check_for_pdfs()

    def check_for_pdfs(self):
        for filename in os.listdir(self.directory_to_watch):
            if filename.endswith(".pdf"):
                file_path = os.path.join(self.directory_to_watch, filename)
                print(f"File PDF baru ditemukan: {file_path}")
                print_pdf(file_path)
                os.remove(file_path)
                print(f"File dihapus: {file_path}")

def set_default_printer(printer_name):
    try:
        win32print.SetDefaultPrinter(printer_name)
        print(f"Printer default diatur ke: {printer_name}")
    except Exception as e:
        print(f"Error saat mengatur printer default: {e}")

def print_pdf(file_path):
    # Gantil path Foxit Reader sesuai dengan lokasi di komputer masing2
    foxit_reader_path = r"C:\Program Files (x86)\Foxit Software\Foxit PDF Editor\FoxitPDFEditor.exe"
    
    # Mengatur parameter cetak untuk ukuran A4 dan multiple pages per sheet (2 halaman per sheet)
    # Catatan: Sesuaikan parameter sesuai dengan dokumentasi Foxit Reader
    print_command = f'"{foxit_reader_path}" /t "{file_path}" /size A4 /n-up 2x1 /fit-to-page'
    
    print(f"Menjalankan perintah print: {print_command}")
    try:
        result = subprocess.run(print_command, shell=True, capture_output=True, text=True)
        print(f"Output perintah: {result.stdout}")
        if result.stderr:
            print(f"Error perintah: {result.stderr}")
    except Exception as e:
        print(f"Error saat menjalankan perintah print: {e}")

if __name__ == '__main__':
    path_to_watch = r'C:\Users\DELL\Downloads'  # Ganti dengan path folder unduhan Anda

    # Ganti dengan nama printer yang ingin dijadikan default
    desired_printer = "EPSON L3250 Series"
    set_default_printer(desired_printer)

    w = Watcher(path_to_watch)
    w.run()
