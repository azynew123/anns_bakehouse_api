import os
import time
import subprocess
import fitz  # PyMuPDF
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class Watcher:
    def __init__(self, directory_to_watch, printer_name):
        self.directory_to_watch = directory_to_watch
        self.printer_name = printer_name
        self.observer = Observer()

    def run(self):
        event_handler = Handler(self.directory_to_watch, self.printer_name)
        self.observer.schedule(event_handler, self.directory_to_watch, recursive=True)
        self.observer.start()
        print(f"Observer dijalankan untuk folder: {self.directory_to_watch}")
        try:
            while True:
                time.sleep(2)
        except KeyboardInterrupt:
            self.observer.stop()
            print("Observer dihentikan")
        self.observer.join()

class Handler(FileSystemEventHandler):
    def __init__(self, directory_to_watch, printer_name):
        self.directory_to_watch = directory_to_watch
        self.printer_name = printer_name

    def on_created(self, event):
        print(f"Peristiwa terdeteksi: {event}")
        if event.is_directory:
            print("Peristiwa diabaikan karena direktori")
            return None
        else:
            print(f"Menunggu sebentar untuk memeriksa file: {event.src_path}")
            time.sleep(2)  # Waktu tunggu untuk memastikan file PDF telah selesai diunduh
            self.check_for_pdfs()

    def check_for_pdfs(self):
        for filename in os.listdir(self.directory_to_watch):
            if filename.endswith(".pdf"):
                file_path = os.path.join(self.directory_to_watch, filename)
                print(f"File PDF baru ditemukan: {file_path}")
                self.process_pdf(file_path)
                os.remove(file_path)
                print(f"File dihapus: {file_path}")

    def process_pdf(self, file_path):
        # Memecah PDF menjadi halaman individu
        pdf_paths = split_pdf(file_path, self.directory_to_watch)
        for pdf_path in pdf_paths:
            print_pdf(pdf_path, self.printer_name)
            os.remove(pdf_path)  # Hapus file setelah dicetak

def set_default_printer(printer_name):
    try:
        subprocess.run(['lpoptions', '-d', printer_name], check=True)
        print(f"Printer default diatur ke: {printer_name}")
    except subprocess.CalledProcessError as e:
        print(f"Error saat mengatur printer default: {e}")

def split_pdf(file_path, output_dir):
    """
    Memecah PDF menjadi file PDF terpisah untuk setiap halaman.
    """
    pdf_document = fitz.open(file_path)
    pdf_pages = len(pdf_document)
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    pdf_paths = []
    
    for page_number in range(pdf_pages):
        pdf_writer = fitz.open()  # Buat dokumen PDF kosong
        pdf_writer.insert_pdf(pdf_document, from_page=page_number, to_page=page_number)
        output_path = os.path.join(output_dir, f'page_{page_number + 1}.pdf')
        pdf_writer.save(output_path)
        pdf_writer.close()
        pdf_paths.append(output_path)
    
    pdf_document.close()
    return pdf_paths

def print_pdf(file_path, printer_name):
    """
    Mencetak PDF dan mengirim perintah pemotongan otomatis ke printer TMA82.
    """
    print_command = f'lp -d "{printer_name}" -o media=A4 -n 1 -o collate=false "{file_path}"'
    try:
        result = subprocess.run(print_command, shell=True, capture_output=True, text=True)
        print(f"Output perintah: {result.stdout}")
        if result.stderr:
            print(f"Error perintah: {result.stderr}")

        # Kirim perintah pemotongan ke printer setelah file PDF dicetak
        cut_paper_command = f'echo -e "\\x1D\\x56\\x00" | lp -d "{printer_name}"'
        print(f"Menjalankan perintah pemotongan: {cut_paper_command}")
        cut_result = subprocess.run(cut_paper_command, shell=True, capture_output=True, text=True)
        print(f"Output perintah pemotongan: {cut_result.stdout}")
        if cut_result.stderr:
            print(f"Error perintah pemotongan: {cut_result.stderr}")
    except Exception as e:
        print(f"Error saat menjalankan perintah print: {e}")

if __name__ == '__main__':
    path_to_watch = '/Users/admkitchen/Downloads'
    desired_printer = "EPSON_TM_T82_S_A_2"
    set_default_printer(desired_printer)
    w = Watcher(path_to_watch, desired_printer)
    w.run()
