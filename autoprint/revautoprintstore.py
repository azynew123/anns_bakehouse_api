import os
import time
import subprocess
import platform
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class Watcher:
    def __init__(self, directory_to_watch):
        self.directory_to_watch = directory_to_watch
        self.observer = Observer()

    def run(self):
        event_handler = Handler(self.directory_to_watch)
        self.observer.schedule(event_handler, self.directory_to_watch, recursive=True)
        self.observer.start()
        print(f"Observer dijalankan untuk folder: {self.directory_to_watch}")
        try:
            while True:
                time.sleep(5)
        except KeyboardInterrupt:
            self.observer.stop()
            print("Observer dihentikan")
        self.observer.join()

class Handler(FileSystemEventHandler):
    def __init__(self, directory_to_watch):
        self.directory_to_watch = directory_to_watch

    def on_created(self, event):
        print(f"Peristiwa terdeteksi: {event}")
        if event.is_directory:
            print("Peristiwa diabaikan karena direktori")
            return None
        else:
            print(f"Menunggu sebentar untuk memeriksa file: {event.src_path}")
            time.sleep(5)  # Waktu tunggu untuk memastikan file PDF telah selesai diunduh
            self.check_for_pdfs()

    def check_for_pdfs(self):
        for filename in os.listdir(self.directory_to_watch):
            if filename.endswith(".pdf"):
                file_path = os.path.join(self.directory_to_watch, filename)
                print(f"File PDF baru ditemukan: {file_path}")
                print_pdf(file_path, desired_printer)  # Menggunakan printer default yang telah ditentukan
                os.remove(file_path)
                print(f"File dihapus: {file_path}")

def set_default_printer(printer_name):
    system_platform = platform.system()
    if system_platform == "Darwin":  # macOS
        try:
            subprocess.run(['lpoptions', '-d', printer_name], check=True)
            print(f"Printer default diatur ke: {printer_name}")
        except subprocess.CalledProcessError as e:
            print(f"Error saat mengatur printer default: {e}")
    elif system_platform == "Windows":
        try:
            subprocess.run(['RUNDLL32 PRINTUI.DLL,PrintUIEntry /y /n', printer_name], shell=True, check=True)
            print(f"Printer default diatur ke: {printer_name}")
        except subprocess.CalledProcessError as e:
            print(f"Error saat mengatur printer default: {e}")

def print_pdf(file_path, printer_name):
    system_platform = platform.system()
    if system_platform == "Darwin":  # macOS
        print_command = f'lp -d "{printer_name}" -o media=A5 -n 2 -o collate=false "{file_path}"'
    elif system_platform == "Windows":
        acrobat_path = r'C:\Program Files\Adobe\Acrobat DC\Acrobat\Acrobat.exe'  # Path ke Acrobat.exe
        print_command = f'"{acrobat_path}" /t "{file_path}" "{printer_name}"'
        print_command += " & "  # Menambahkan tanda '&' untuk menjalankan perintah berikutnya jika diperlukan

    print(f"Menjalankan perintah print: {print_command}")
    try:
        result = subprocess.run(print_command, shell=True, capture_output=True, text=True)
        print(f"Output perintah: {result.stdout}")
        if result.stderr:
            print(f"Error perintah: {result.stderr}")
    except Exception as e:
        print(f"Error saat menjalankan perintah print: {e}")

if __name__ == '__main__':
    # Ganti dengan path folder unduhan Anda
    path_to_watch = '/Users/imac/Downloads' if platform.system() == "Darwin" else r'C:\Users\DELL\Downloads'

    # Ganti dengan nama printer yang ingin dijadikan default
    desired_printer = "L3250 Series(Network)"
    set_default_printer(desired_printer)

    w = Watcher(path_to_watch)
    w.run()


    # desired_printer = "EPSON L3250 Series"
    # set_default_printer(desired_printer)

    # w = Watcher(path_to_watch)
    # w.run()
