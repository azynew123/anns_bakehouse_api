
import xmlrpc.client
import csv
from io import StringIO
from google.cloud import storage
import json

url = 'https://annsbakehouse.odoo.com/'
db = 'odoo-ps-pshk-berawal-dari-kue-main-8753418'
username = 'anns@annsbakehouse.com'
password = 'Trr3SLECHES!'


# info = xmlrpc.client.ServerProxy('https://demo.odoo.com/start').start()
# url, db, username, password = info['host'], info['database'], info['user'], info['password']

common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
version=common.version()
print("details..", version)

uid = common.authenticate(db, username, password, {})
print("UID = ", uid)


models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
partners=models.execute_kw(db, uid, password, 'res.partner', 'check_access_rights', ['read'], {'raise_exception': False})
partners_detail=models.execute_kw(db, uid, password, 'res.partner', 'search',[[]])
print("partners = ", partners)
print("partners Details :", partners_detail)

sales=models.execute_kw(db, uid, password, 'sale.order.line', 'check_access_rights', ['read'], {'raise_exception': False})
sales_detail=models.execute_kw(db, uid, password, 'sale.order.line', 'search',[[]])
print("sales = ", sales)
print("sales Details :", sales_detail)

models.execute_kw(db, uid, password, 'res.partner', 'search', [[['is_company', '=', True]]])
partner_ids=models.execute_kw(db, uid, password, 'res.partner', 'search', [[['is_company', '=', True]]], {'offset': 0, 'limit': 10})
print("Partenr Ids :", partner_ids)

ids = models.execute_kw(db, uid, password, 'res.partner', 'search', [[['is_company', '=', True]]], {'limit': 1})
partner_rec = models.execute_kw(db, uid, password, 'res.partner', 'read', [partner_ids],{'fields': ['id','name']})
for partner in partner_rec:
    print(partner)
 
# sales_ids=models.execute_kw(db, uid, password, 'sale.order.line', 'search', [[['is_company', '=', True]]], {'offset': 0, 'limit': 10})
# sales_order_line_rec = models.execute_kw(db, uid, password, 'sale.order.line', 'read', [partner_ids],{'fields': ['state','order_id','x_studio_sales_type','x_studio_order_channel','x_studio_occasion_tags','x_studio_warehouse','x_studio_created_date','x_studio_delivery_date','create_date','x_studio_related_field_v7J68','order_partner_id','x_studio_related_field_581Ww','x_studio_product_category','product_template_id','product_id','name','salesman_id','product_uom_qty','price_unit','discount','price_subtotal']})
# # print("partner rec", partner_rec)
# for sale_order_line in sales_order_line_rec:
#     print(sale_order_line)
    


models.execute_kw(db, uid, password, 'res.partner', 'search_count', [[['is_company', '=', True]]])
partners_count= models.execute_kw(db, uid, password, 'res.partner', 'search_count', [[]])
print("Partners Count:", partners_count)


# Inisialisasi klien Cloud Storage
client_gcs = storage.Client.from_service_account_json('annsbakehouse-prd-823739eceedf.json')

# Tentukan bucket dan nama file tujuan di Cloud Storage
bucket_name = 'odoo_api_test'
destination_blob_name = 'odoo_api_test/new.csv'


data = str(partner_rec)  # Misalnya, mengonversi data Odoo ke format string
json_data_with_double_quotes = data.replace("'", '"')



# Upload file ke Cloud Storage
bucket_gcs = client_gcs.get_bucket(bucket_name)
blob = bucket_gcs.blob(destination_blob_name)
blob.upload_from_string(data, content_type='application/csv')
# blob.upload_from_filename(local_file_path)

print('Data berhasil diunggah ke Google Cloud Storage.')

# count the number of fields fetched by default
# len(partner_rec)