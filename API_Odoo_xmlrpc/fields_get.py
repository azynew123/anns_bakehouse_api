
import xmlrpc.client
# import csv
# from io import StringIO
from google.cloud import storage
# import json
import pandas as pd
from datetime import datetime, timedelta

# Mendapatkan waktu saat ini
waktu_sekarang = datetime.now()
print("Waktu Sekarang:", waktu_sekarang)

# Format waktu sebagai string
waktu_format_string = waktu_sekarang.strftime("%Y-%m-%d %H:%M:%S")
print("Waktu dalam Format String:", waktu_format_string)

# Mendapatkan komponen waktu tertentu
tahun = waktu_sekarang.year
bulan = waktu_sekarang.month
hari = waktu_sekarang.day
jam = waktu_sekarang.hour
menit = waktu_sekarang.minute
detik = waktu_sekarang.second

tanggal_str = f"{tahun}{bulan}{hari}{jam}{menit}{detik}"
waktu_str=f"{waktu_sekarang}"

# Mengatur waktu mulai hari ini pukul 00:00:00
waktu_mulai = datetime(waktu_sekarang.year, waktu_sekarang.month, waktu_sekarang.day, jam - 7 - 1, 0, 0)

waktu_update = datetime(waktu_sekarang.year, waktu_sekarang.month, waktu_sekarang.day, jam - 7 - 1, 59, 59)


url = 'https://annsbakehouse.odoo.com/'
db = 'odoo-ps-pshk-berawal-dari-kue-main-8753418'
username = 'anns@annsbakehouse.com'
password = 'Trr3SLECHES!'

# Inisialisasi klien Cloud Storage
client_gcs = storage.Client.from_service_account_json('annsbakehouse-prd-823739eceedf.json')
# info = xmlrpc.client.ServerProxy('https://demo.odoo.com/start').start()
# url, db, username, password = info['host'], info['database'], info['user'], info['password']

common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
version=common.version()
print("details..", version)

uid = common.authenticate(db, username, password, {})
print("UID = ", uid)


models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
# Panggil metadata field dari model yang diinginkan
model_fields = models.execute_kw(db, uid, password, 'sale.order.line', 'fields_get',[[]])

# Ambil nama-nama field
fields_to_fetch = list(model_fields.keys())

# fields_to_fetch = ['id','x_studio_created_date','create_date','write_date','order_partner_id','x_studio_related_field_v7J68','x_studio_delivery_date','x_studio_related_field_581Ww','name','discount','x_studio_occasion_tags', 'x_studio_order_channel','order_id','state','product_id','x_studio_product_category','product_template_id','product_uom_qty','x_studio_sales_team_pic','x_studio_sales_type','salesman_id','price_subtotal','price_unit','x_studio_warehouse']

sales=models.execute_kw(db, uid, password, 'sale.order.line', 'check_access_rights', ['read'], {'raise_exception': False})
sales_detail=models.execute_kw(db, uid, password, 'sale.order.line', 'search',[[]])
print("sales = ", sales)
print("sales Details :", sales_detail)



# Membuat domain untuk pencarian berdasarkan waktu
domain = [['write_date', '>=', waktu_mulai.strftime("%Y-%m-%d %H:%M:%S")],
          ['write_date', '<=', waktu_update.strftime("%Y-%m-%d %H:%M:%S")]]

sales_count= models.execute_kw(db, uid, password, 'sale.order.line', 'search_count', [domain])#Ganti Variabel Date
print("sales Count:", sales_count)


ids = models.execute_kw(db, uid, password, 'sale.order.line', 'search', [domain]) #Ganti Variabel Date
sales_order_line_rec = models.execute_kw(db, uid, password, 'sale.order.line', 'read', [ids],{'fields': fields_to_fetch})
# sales_order_line_rec = models.execute_kw(db, uid, password, 'sale.order.line', 'read', [ids],{})

df = pd.DataFrame(sales_order_line_rec)

# Memisahkan nilai list dalam field 'partner_id' menjadi dua kolom baru
df[['field_order_partner_id', 'field_order_partner_id_name']] = pd.DataFrame(df['order_partner_id'].apply(pd.Series))
df[['field_order_id', 'field_order_id_name']] = pd.DataFrame(df['order_id'].apply(pd.Series))
df[['field_x_studio_product_category_id', 'field_x_studio_product_category_id_name']] = pd.DataFrame(df['x_studio_product_category'].apply(pd.Series))
df[['field_product_template_id', 'field_product_template_id_name']] = pd.DataFrame(df['product_template_id'].apply(pd.Series))
df[['field_salesman_id', 'field_salesman_id_name']] = pd.DataFrame(df['salesman_id'].apply(pd.Series))
df[['field_x_studio_warehouse_id', 'field_x_studio_warehouse_id_name']] = pd.DataFrame(df['x_studio_warehouse'].apply(pd.Series))

# Rename nama field sesuai yang Anda inginkan
df.rename(columns={'id':'ID','x_studio_created_date':'Created_Date','create_date':'Created_On','write_date':'Last_Modified_On','field_order_partner_id':'Customer_ID','field_order_partner_id_name':'Customer_ID_Name','x_studio_related_field_v7J68':'Customer_ID','x_studio_delivery_date':'Delivery_Date','x_studio_related_field_581Ww':'Delivery_Zip_Code','name':'Description','discount':'Discount(%)','x_studio_occasion_tags':'Occasion_Tags','x_studio_order_channel':'Order_Channel','field_order_id': 'Order_Reference_ID','field_order_id_name': 'Order_Reference_ID_Name','state': 'Order_Status', 'product_id':'Product','field_x_studio_product_category_id':'Product_Category_ID','field_x_studio_product_category_id_name':'Product_Category_ID_Name','field_product_template_id':'Product_Template_ID','field_product_template_id_name':'Product_Template_ID_Name','product_uom_qty':'Quantity','x_studio_sales_team_pic':'Sales_Team_PIC', 'x_studio_sales_type':'Sales_Type', 'field_salesman_id':'Salesperson_ID', 'field_salesman_id_name':'Salesperson_ID_Name','price_subtotal':'Subtotal', 'price_unit':'Unit_Price','field_x_studio_warehouse_id':'Warehouse_ID','field_x_studio_warehouse_id_name':'Warehouse_ID_Name'}, inplace=True)#Tambahin Field __last_update

# Hapus kolom 'partner_id' yang asli jika diinginkan
df.drop('order_partner_id', axis=1, inplace=True)
df.drop('order_id', axis=1, inplace=True)
df.drop('x_studio_product_category', axis=1, inplace=True)
df.drop('product_template_id', axis=1, inplace=True)
df.drop('salesman_id', axis=1, inplace=True)
df.drop('x_studio_warehouse', axis=1, inplace=True)




# Simpan DataFrame ke file CSV
csv_data = df.to_csv(index=False)

# Simpan data CSV di Google Cloud Storage
# yyyymmddhhmmss
gcs_bucket_name = 'odoo-csv'
gcs_file_name = 'test_split_value/api_sales_order_line/sales_order_line_' + tanggal_str + '.csv'#Ganti Variabel Date

client = storage.Client.from_service_account_json('annsbakehouse-prd-823739eceedf.json')
bucket = client.bucket(gcs_bucket_name)
blob = bucket.blob(gcs_file_name)

# Upload data CSV ke GCS
blob.upload_from_string(csv_data, content_type='text/csv')

print(df)

print("\nWaktu Mulai", waktu_mulai)
print("Waktu Update File", waktu_update.strftime("%Y-%m-%d %H:%M:%S"))
print("Waktu Sekarang", waktu_sekarang.strftime("%Y-%m-%d %H:%M:%S"))
print("sales Count:", sales_count)
print(f'Data berhasil disimpan di GCS: gs://{gcs_bucket_name}/{gcs_file_name}')

