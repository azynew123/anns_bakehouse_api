import requests
from google.cloud import storage
import json
import logging

def get_customer_data_from_odoo():
    odoo_api_url = 'https://your-odoo-instance.com/api/customer'
    headers = {'Authorization': 'Bearer your_odoo_api_token'}
    response = requests.get(odoo_api_url, headers=headers)
    
    if response.status_code == 200:
        logging.info('Data successfully retrieved from Odoo.')
        return response.json()
    else:
        logging.error(f'Failed to retrieve data from Odoo. Status code: {response.status_code}')
        return None

def upload_customer_data_to_gcs(customer_data):
    client = storage.Client.from_service_account_json('path/to/your/credentials.json')
    bucket_name = 'your_bucket_name'
    file_name = 'customer_data.json'

    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(file_name)

    try:
        blob.upload_from_string(json.dumps(customer_data))
        logging.info('Data successfully uploaded to Google Cloud Storage.')
    except Exception as e:
        logging.error(f'Failed to upload data to Google Cloud Storage. Error: {str(e)}')

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    
    odoo_data = get_customer_data_from_odoo()

    if odoo_data:
        upload_customer_data_to_gcs(odoo_data)
    else:
        logging.error('Data upload to Google Cloud Storage skipped due to failure in Odoo data retrieval.')
