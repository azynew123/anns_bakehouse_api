
import xmlrpc.client
# import csv
# from io import StringIO
from google.cloud import storage
# import json
import pandas as pd
from datetime import datetime, timedelta

# Mendapatkan waktu saat ini
waktu_sekarang = datetime.now()
print("Waktu Sekarang:", waktu_sekarang)

# Format waktu sebagai string
waktu_format_string = waktu_sekarang.strftime("%Y-%m-%d %H:%M:%S")
print("Waktu dalam Format String:", waktu_format_string)

# Mendapatkan komponen waktu tertentu
tahun = waktu_sekarang.year
bulan = waktu_sekarang.month
hari = waktu_sekarang.day
jam = waktu_sekarang.hour
menit = waktu_sekarang.minute
detik = waktu_sekarang.second

tanggal_str = f"{tahun}{bulan}{hari}{jam}{menit}{detik}"
waktu_str=f"{waktu_sekarang}"



url = 'https://annsbakehouse.odoo.com/'
db = 'odoo-ps-pshk-berawal-dari-kue-main-8753418'
username = 'anns@annsbakehouse.com'
password = 'Trr3SLECHES!'

# Inisialisasi klien Cloud Storage
client_gcs = storage.Client.from_service_account_json("Refactor/annsbakehouse-prd-823739eceedf.json")
# info = xmlrpc.client.ServerProxy('https://demo.odoo.com/start').start()
# url, db, username, password = info['host'], info['database'], info['user'], info['password']

# # Fungsi untuk menghapus blob dari GCS
# def delete_all_blobs_in_path(bucket_name, path):
#     """Hapus semua blob di path tertentu dalam bucket GCS."""
#     storage_client = storage.Client.from_service_account_json('annsbakehouse-prd-823739eceedf.json')
#     bucket = storage_client.get_bucket(bucket_name)

#     # Dapatkan daftar blob di path tertentu dalam bucket
#     blobs = bucket.list_blobs(prefix=path)

#     # Iterasi melalui daftar blob dan hapus masing-masing
#     for blob in blobs:
#         blob.delete()
#         print(f'Blob {blob.name} berhasil dihapus dari bucket {bucket_name}.')
   
#         # blob_date = datetime.strptime(blob.name.split('_')[6].split('.')[0], "%Y%m%d%H%M%S")
        
#         # if (datetime.now() - blob_date).days <= 0:
#         #     blob.delete()
#         #     print(f'Blob {blob.name} berhasil dihapus dari bucket {bucket_name}.')
  

        

# Fungsi untuk mengunggah DataFrame ke GCS sebagai file CSV
def upload_blob_from_dataframe(bucket_name, blob_name, dataframe):
    """Unggah DataFrame ke GCS sebagai file CSV."""
    storage_client = storage.Client.from_service_account_json('Refactor/annsbakehouse-prd-823739eceedf.json')
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)

    # Menggunakan metode to_csv dari Pandas untuk menyimpan DataFrame ke string CSV
    csv_data = dataframe.to_csv(index=False)
    
    # Mengunggah string CSV ke GCS
    blob.upload_from_string(csv_data, content_type='text/csv')
    print(f'\nBlob {blob_name} berhasil diunggah ke bucket {bucket_name}.')

# ... (kode lainnya)



# Tanggal saat ini
current_date = datetime.now()


common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
version=common.version()


uid = common.authenticate(db, username, password, {})



models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))


# # Panggil metadata field dari model yang diinginkan
# model_fields = models.execute_kw(db, uid, password, 'sale.order.line', 'fields_get',[[]])


# fields_to_fetch = list(model_fields.keys())


fields_to_fetch = ['id','x_studio_created_date','create_date','write_date','order_partner_id','x_studio_related_field_v7J68','x_studio_delivery_date','x_studio_related_field_581Ww','name','discount','x_studio_occasion_tags', 'x_studio_order_channel','order_id','state','product_id','x_studio_product_category','product_template_id','product_uom_qty','x_studio_sales_team_pic','x_studio_sales_type','salesman_id','price_subtotal','price_unit','x_studio_warehouse']

sales=models.execute_kw(db, uid, password, 'sale.order.line', 'check_access_rights', ['read'], {'raise_exception': False})
sales_detail=models.execute_kw(db, uid, password, 'sale.order.line', 'search',[[]])


# # Mengatur waktu mulai hari ini pukul 00:00:00
# waktu_mulai_00 = datetime(waktu_sekarang.year, waktu_sekarang.month, waktu_sekarang.day, 0, 0, 0)
# Mengatur waktu mulai hari ini pukul 00:00:00
waktu_mulai = waktu_sekarang  - timedelta(hours=176)

waktu_update = waktu_sekarang - timedelta(hours=8)
# waktu_mulai = waktu_update.replace(minute=0, second=0)
waktu_update = waktu_update.replace(minute=59, second=59)
# Membuat domain untuk pencarian berdasarkan waktu
# Membuat domain untuk pencarian berdasarkan waktu
domain = [['x_studio_delivery_date', '>=', waktu_mulai.strftime("%Y-%m-%d %H:%M:%S")]]



model_fields = models.execute_kw(db, uid, password, 'sale.order.line', 'fields_get', [[]])
fields_to_fetch = list(model_fields.keys())

sales_count= models.execute_kw(db, uid, password, 'sale.order.line', 'search_count', [domain])#Ganti Variabel Date
print("Sales Count", sales_count)

ids = models.execute_kw(db, uid, password, 'sale.order.line', 'search', [domain]) #Ganti Variabel Date
sales_order_line_rec = models.execute_kw(db, uid, password, 'sale.order.line', 'read', [ids],{'fields': fields_to_fetch})
# sales_order_line_rec = models.execute_kw(db, uid, password, 'sale.order.line', 'read', [ids],{})
# Iterasi melalui setiap record hasil pembacaan
# Panggil metadata field dari model yang diinginkan


df = pd.DataFrame(sales_order_line_rec)

# df.rename(columns={'id':'ID','x_studio_created_date':'Created_Date','create_date':'Created_On','write_date':'Last_Modified_On','field_order_partner_id':'Customer_ID','field_order_partner_id_name':'Customer_ID_Name','x_studio_related_field_v7J68':'Customer_ID','x_studio_delivery_date':'Delivery_Date','x_studio_related_field_581Ww':'Delivery_Zip_Code','name':'Description','discount':'Discount(%)','x_studio_occasion_tags':'Occasion_Tags','x_studio_order_channel':'Order_Channel','field_order_id': 'Order_Reference_ID','field_order_id_name': 'Order_Reference_ID_Name','state': 'Order_Status', 'product_id':'Product','field_x_studio_product_category_id':'Product_Category_ID','field_x_studio_product_category_id_name':'Product_Category_ID_Name','field_product_template_id':'Product_Template_ID','field_product_template_id_name':'Product_Template_ID_Name','product_uom_qty':'Quantity','x_studio_sales_team_pic':'Sales_Team_PIC', 'x_studio_sales_type':'Sales_Type', 'field_salesman_id':'Salesperson_ID', 'field_salesman_id_name':'Salesperson_ID_Name','price_subtotal':'Subtotal', 'price_unit':'Unit_Price','field_x_studio_warehouse_id':'Warehouse_ID','field_x_studio_warehouse_id_name':'Warehouse_ID_Name'}, inplace=True)#Tambahin Field __last_update
df['dm_created_datetime'] = pd.to_datetime('today')

# Iterasi melalui setiap field yang perlu diubah
for field in fields_to_fetch:
    # Periksa apakah field memiliki tipe data many2one
    if model_fields[field]['type'] == 'many2one':
        # Split nilai list menggunakan Pandas apply(pd.Series)
        many2one_split = df[field].apply(pd.Series)
        
        # Cek panjang setiap kolom hasil split
        if len(many2one_split.columns) == 2:
            # Jika panjangnya konsisten, tambahkan ke DataFrame
            df[[f'{field}_id', f'{field}_name']] = many2one_split
            # Hapus kolom asli yang memiliki nilai list
            df = df.drop(columns=[field])
        else:
            print(f"Kendala: Panjang list pada field {field} tidak konsisten.")




df.rename(columns={'id':'ID','x_studio_created_date':'Created_Date','create_date':'Created_On','write_date':'Last_Modified_On','field_order_partner_id':'Customer_ID','field_order_partner_id_name':'Customer_ID_Name','x_studio_related_field_v7J68':'Customer_ID','x_studio_delivery_date':'Delivery_Date','x_studio_related_field_581Ww':'Delivery_Zip_Code','name':'Description','discount':'Discount(%)','x_studio_occasion_tags':'Occasion_Tags','x_studio_order_channel':'Order_Channel','field_order_id': 'Order_Reference_ID','field_order_id_name': 'Order_Reference_ID_Name','state': 'Order_Status','field_x_studio_product_category_id':'Product_Category_ID','field_x_studio_product_category_id_name':'Product_Category_ID_Name','field_product_template_id':'Product_Template_ID','field_product_template_id_name':'Product_Template_ID_Name','product_uom_qty':'Quantity','x_studio_sales_team_pic':'Sales_Team_PIC', 'x_studio_sales_type':'Sales_Type', 'field_salesman_id':'Salesperson_ID', 'field_salesman_id_name':'Salesperson_ID_Name','price_subtotal':'Subtotal', 'price_unit':'Unit_Price','field_x_studio_warehouse_id':'Warehouse_ID','field_x_studio_warehouse_id_name':'Warehouse_ID_Name'}, inplace=True)#Tambahin Field __last_update

# Simpan DataFrame ke file CSV
csv_data = df.to_csv(index=False)



# Simpan data CSV di Google Cloud Storage
# yyyymmddhhmmss
gcs_bucket_name = 'odoo_api_test_refactor'
gcs_file_name = 'ref_sales_order_line_stg/ref_sales_order_line_' + tanggal_str + '.csv'#Ganti Variabel Date
gcs_path = 'ref_sales_order_line_stg/' 


print('\n\nWaktu Mulai Ambil  Record: ', waktu_mulai)
print('Waktu Update: ', waktu_update)
print('Waktu Sekarang: ', waktu_sekarang)
print("Sales Count : ", sales_count,'\n')

# delete_all_blobs_in_path(gcs_bucket_name, gcs_path)

upload_blob_from_dataframe(gcs_bucket_name, gcs_file_name,df)
            
        

